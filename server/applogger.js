//-- Outer level logger
var bunyan = require( 'bunyan' );

//-- Logger
exports.logger = bunyan.createLogger({ name: 'node_starter' });