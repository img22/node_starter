var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var jadeStatic = require('jade-static');

var routes = require('./routes/index');

var applogger = require('./applogger');
var log = applogger.logger.child({
    component: 'app'
});

if(!process.env.SECRET || !process.env.SECRET.length)
    throw new Error('Need SECRET to run app');

if(!process.env.DB || !process.env.DB.length)
    throw new Error('Need DB to run app');

if(!process.env.KEY_LOC || !process.env.KEY_LOC.length)
    throw new Error('Need KEY_LOC for SSL');

if(!process.env.CERT_LOC || !process.env.CERT_LOC.length)
    throw new Error('Need CERT_LOC for SSL');

if(!process.env.NODE_ENV)
    throw new Error('Need NODE_ENV to run app');

if(process.env.NODE_ENV !== 'dev' &&
   process.env.NODE_ENV !== 'stag' &&
   process.env.NODE_ENV !== 'int' &&
   process.env.NODE_ENV !== 'prod')
    throw new Error('Invalid NODE_ENV');

if(!process.env.APP_HOSTNAME)
    throw new Error('Need APP_HOSTNAME to run app');

var app = express();


//-- uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({
    limit: '50mb',
    extended: false
}));


//-- serve the client
app.set('view engine', 'jade');
app.set('views', path.join(__dirname, '../client/views'));
app.use(express.static(path.join(__dirname, '../client/public')));

/* error handlers */
//-- development error handler
//-- will print stacktrace
if(process.env.NODE_ENV != 'prod') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
} else {
    //-- production error handler
    //-- no stacktraces leaked to user
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: {}
        });
    });
}

//-- Init database and make it accessible to request
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/' + process.env.DB);
log.info('connected to ' + mongoose.connection.db.databaseName);

app.use(function(req, res, next) {
    req.db = mongoose;
    next();
});

//-- server side routes
app.get('/', routes.routeToIndex);
app.get('/partials/:name', routes.routeToPartials);


module.exports = app;
